﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Services
{
    public interface IHttpContextService
    {
        string GetFullRequestUrl();
        string GetBaseRequestUrl();
    }
}
