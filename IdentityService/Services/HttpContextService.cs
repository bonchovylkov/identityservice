﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Services
{
    public class HttpContextService : IHttpContextService
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public HttpContextService(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
        }

        public string GetBaseRequestUrl()
        {

            var httpContext = _contextAccessor.HttpContext;
            var url = $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";
            return url;
        }

        public string GetFullRequestUrl()
        {
            var httpContext = _contextAccessor.HttpContext;
            var url = $"{httpContext.Request.Scheme}://{httpContext.Request.Host}";
            return url;
        }
    }
}
