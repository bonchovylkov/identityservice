﻿
using IdentityServer4.Models;

namespace IdentityService.Models
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}